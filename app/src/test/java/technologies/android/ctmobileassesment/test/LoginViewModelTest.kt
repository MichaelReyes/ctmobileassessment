package technologies.android.ctmobileassesment.test

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.EmptyResultSetException
import com.google.gson.Gson
import com.jakewharton.rxrelay2.PublishRelay
import com.orhanobut.hawk.Hawk
import io.mockk.MockK
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.internal.schedulers.ExecutorScheduler.*
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import technologies.android.ctmobileassesment.core.extension.fromJson
import technologies.android.ctmobileassesment.core.extension.getOrAwaitValue
import technologies.android.ctmobileassesment.core.model.Constants
import technologies.android.ctmobileassesment.core.model.db.data.UserData
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.authentication.LoginSignUpRequest
import technologies.android.ctmobileassesment.core.network.repository.AuthenticationRepository
import technologies.android.ctmobileassesment.feature.authentication.login.LoginViewModel
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    private var mContextMock: Context? = null
    private var repo: AuthenticationRepository? = null
    private var vm: LoginViewModel? = null

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val immediateScheduler: Scheduler = object : Scheduler() {

        override fun createWorker() = ExecutorWorker(Executor { it.run() }, true)

        // This prevents errors when scheduling a delay
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            return super.scheduleDirect(run, 0, unit)
        }

    }

    @Before
    fun setup() {
        RxJavaPlugins.setIoSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { immediateScheduler }

        mContextMock = mockk(relaxed = true)
        repo = mockk(relaxed = true)
        vm = LoginViewModel(mContextMock!!, repo!!)
    }

    @After
    fun tearDown() {
        mContextMock = null
        repo = null
        vm = null

        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }


    @Test fun successGetCountries(){
        vm?.apply {
            every { repo?.getCountries() } returns
                    Single.just(Gson().fromJson("[{\"name\": \"Afghanistan\",\"topLevelDomain\": [\".af\"],\"alpha2Code\": \"AF\",\"alpha3Code\": \"AFG\",\"callingCodes\": [\"93\"],\"capital\": \"Kabul\",\"altSpellings\": [\"AF\",\"Afġānistān\"],\"region\": \"Asia\",\"subregion\": \"Southern Asia\",\"population\": 27657145,\"latlng\": [33,65],\"demonym\": \"Afghan\",\"area\": 652230,\"gini\": 27.8,\"timezones\": [\"UTC+04:30\"],\"borders\": [\"IRN\",\"PAK\",\"TKM\",\"UZB\",\"TJK\",\"CHN\"],\"nativeName\": \"افغانستان\",\"numericCode\": \"004\",\"currencies\": [{\"code\": \"AFN\",\"name\": \"Afghan afghani\",\"symbol\": \"؋\"}],\"languages\": [{\"iso639_1\": \"ps\",\"iso639_2\": \"pus\",\"name\": \"Pashto\",\"nativeName\": \"پښتو\"},{\"iso639_1\": \"uz\",\"iso639_2\": \"uzb\",\"name\": \"Uzbek\",\"nativeName\": \"Oʻzbek\"},{\"iso639_1\": \"tk\",\"iso639_2\": \"tuk\",\"name\": \"Turkmen\",\"nativeName\": \"Türkmen\"}],\"translations\": {\"de\": \"Afghanistan\",\"es\": \"Afganistán\",\"fr\": \"Afghanistan\",\"ja\": \"アフガニスタン\",\"it\": \"Afghanistan\",\"br\": \"Afeganistão\",\"pt\": \"Afeganistão\",\"nl\": \"Afghanistan\",\"hr\": \"Afganistan\",\"fa\": \"افغانستان\"},\"flag\": \"https://restcountries.eu/data/afg.svg\",\"regionalBlocs\": [{\"acronym\": \"SAARC\",\"name\": \"South Asian Association for Regional Cooperation\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"AFG\"},{\"name\": \"Åland Islands\",\"topLevelDomain\": [\".ax\"],\"alpha2Code\": \"AX\",\"alpha3Code\": \"ALA\",\"callingCodes\": [\"358\"],\"capital\": \"Mariehamn\",\"altSpellings\": [\"AX\",\"Aaland\",\"Aland\",\"Ahvenanmaa\"],\"region\": \"Europe\",\"subregion\": \"Northern Europe\",\"population\": 28875,\"latlng\": [60.116667,19.9],\"demonym\": \"Ålandish\",\"area\": 1580,\"gini\": null,\"timezones\": [\"UTC+02:00\"],\"borders\": [],\"nativeName\": \"Åland\",\"numericCode\": \"248\",\"currencies\": [{\"code\": \"EUR\",\"name\": \"Euro\",\"symbol\": \"€\"}],\"languages\": [{\"iso639_1\": \"sv\",\"iso639_2\": \"swe\",\"name\": \"Swedish\",\"nativeName\": \"svenska\"}],\"translations\": {\"de\": \"Åland\",\"es\": \"Alandia\",\"fr\": \"Åland\",\"ja\": \"オーランド諸島\",\"it\": \"Isole Aland\",\"br\": \"Ilhas de Aland\",\"pt\": \"Ilhas de Aland\",\"nl\": \"Ålandeilanden\",\"hr\": \"Ålandski otoci\",\"fa\": \"جزایر الند\"},\"flag\": \"https://restcountries.eu/data/ala.svg\",\"regionalBlocs\": [{\"acronym\": \"EU\",\"name\": \"European Union\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"\"},{\"name\": \"Albania\",\"topLevelDomain\": [\".al\"],\"alpha2Code\": \"AL\",\"alpha3Code\": \"ALB\",\"callingCodes\": [\"355\"],\"capital\": \"Tirana\",\"altSpellings\": [\"AL\",\"Shqipëri\",\"Shqipëria\",\"Shqipnia\"],\"region\": \"Europe\",\"subregion\": \"Southern Europe\",\"population\": 2886026,\"latlng\": [41,20],\"demonym\": \"Albanian\",\"area\": 28748,\"gini\": 34.5,\"timezones\": [\"UTC+01:00\"],\"borders\": [\"MNE\",\"GRC\",\"MKD\",\"KOS\"],\"nativeName\": \"Shqipëria\",\"numericCode\": \"008\",\"currencies\": [{\"code\": \"ALL\",\"name\": \"Albanian lek\",\"symbol\": \"L\"}],\"languages\": [{\"iso639_1\": \"sq\",\"iso639_2\": \"sqi\",\"name\": \"Albanian\",\"nativeName\": \"Shqip\"}],\"translations\": {\"de\": \"Albanien\",\"es\": \"Albania\",\"fr\": \"Albanie\",\"ja\": \"アルバニア\",\"it\": \"Albania\",\"br\": \"Albânia\",\"pt\": \"Albânia\",\"nl\": \"Albanië\",\"hr\": \"Albanija\",\"fa\": \"آلبانی\"},\"flag\": \"https://restcountries.eu/data/alb.svg\",\"regionalBlocs\": [{\"acronym\": \"CEFTA\",\"name\": \"Central European Free Trade Agreement\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"ALB\"}]"))

            getCountries()
            Assert.assertFalse(ui.getOrAwaitValue().countries.isNullOrEmpty())
        }
    }

    @Test fun failGetCountries() {
        vm?.apply {

            val internetConnectionStream = PublishRelay.create<Boolean>()
            setConnectionStream(internetConnectionStream)
            internetConnectionStream.accept(false)

            every { repo?.getCountries() } returns
                    Single.just(
                        Gson().fromJson(
                            "[{\"name\": \"Afghanistan\",\"topLevelDomain\": [\".af\"],\"alpha2Code\": \"AF\",\"alpha3Code\": \"AFG\",\"callingCodes\": [\"93\"],\"capital\": \"Kabul\",\"altSpellings\": [\"AF\",\"Afġānistān\"],\"region\": \"Asia\",\"subregion\": \"Southern Asia\",\"population\": 27657145,\"latlng\": [33,65],\"demonym\": \"Afghan\",\"area\": 652230,\"gini\": 27.8,\"timezones\": [\"UTC+04:30\"],\"borders\": [\"IRN\",\"PAK\",\"TKM\",\"UZB\",\"TJK\",\"CHN\"],\"nativeName\": \"افغانستان\",\"numericCode\": \"004\",\"currencies\": [{\"code\": \"AFN\",\"name\": \"Afghan afghani\",\"symbol\": \"؋\"}],\"languages\": [{\"iso639_1\": \"ps\",\"iso639_2\": \"pus\",\"name\": \"Pashto\",\"nativeName\": \"پښتو\"},{\"iso639_1\": \"uz\",\"iso639_2\": \"uzb\",\"name\": \"Uzbek\",\"nativeName\": \"Oʻzbek\"},{\"iso639_1\": \"tk\",\"iso639_2\": \"tuk\",\"name\": \"Turkmen\",\"nativeName\": \"Türkmen\"}],\"translations\": {\"de\": \"Afghanistan\",\"es\": \"Afganistán\",\"fr\": \"Afghanistan\",\"ja\": \"アフガニスタン\",\"it\": \"Afghanistan\",\"br\": \"Afeganistão\",\"pt\": \"Afeganistão\",\"nl\": \"Afghanistan\",\"hr\": \"Afganistan\",\"fa\": \"افغانستان\"},\"flag\": \"https://restcountries.eu/data/afg.svg\",\"regionalBlocs\": [{\"acronym\": \"SAARC\",\"name\": \"South Asian Association for Regional Cooperation\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"AFG\"},{\"name\": \"Åland Islands\",\"topLevelDomain\": [\".ax\"],\"alpha2Code\": \"AX\",\"alpha3Code\": \"ALA\",\"callingCodes\": [\"358\"],\"capital\": \"Mariehamn\",\"altSpellings\": [\"AX\",\"Aaland\",\"Aland\",\"Ahvenanmaa\"],\"region\": \"Europe\",\"subregion\": \"Northern Europe\",\"population\": 28875,\"latlng\": [60.116667,19.9],\"demonym\": \"Ålandish\",\"area\": 1580,\"gini\": null,\"timezones\": [\"UTC+02:00\"],\"borders\": [],\"nativeName\": \"Åland\",\"numericCode\": \"248\",\"currencies\": [{\"code\": \"EUR\",\"name\": \"Euro\",\"symbol\": \"€\"}],\"languages\": [{\"iso639_1\": \"sv\",\"iso639_2\": \"swe\",\"name\": \"Swedish\",\"nativeName\": \"svenska\"}],\"translations\": {\"de\": \"Åland\",\"es\": \"Alandia\",\"fr\": \"Åland\",\"ja\": \"オーランド諸島\",\"it\": \"Isole Aland\",\"br\": \"Ilhas de Aland\",\"pt\": \"Ilhas de Aland\",\"nl\": \"Ålandeilanden\",\"hr\": \"Ålandski otoci\",\"fa\": \"جزایر الند\"},\"flag\": \"https://restcountries.eu/data/ala.svg\",\"regionalBlocs\": [{\"acronym\": \"EU\",\"name\": \"European Union\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"\"},{\"name\": \"Albania\",\"topLevelDomain\": [\".al\"],\"alpha2Code\": \"AL\",\"alpha3Code\": \"ALB\",\"callingCodes\": [\"355\"],\"capital\": \"Tirana\",\"altSpellings\": [\"AL\",\"Shqipëri\",\"Shqipëria\",\"Shqipnia\"],\"region\": \"Europe\",\"subregion\": \"Southern Europe\",\"population\": 2886026,\"latlng\": [41,20],\"demonym\": \"Albanian\",\"area\": 28748,\"gini\": 34.5,\"timezones\": [\"UTC+01:00\"],\"borders\": [\"MNE\",\"GRC\",\"MKD\",\"KOS\"],\"nativeName\": \"Shqipëria\",\"numericCode\": \"008\",\"currencies\": [{\"code\": \"ALL\",\"name\": \"Albanian lek\",\"symbol\": \"L\"}],\"languages\": [{\"iso639_1\": \"sq\",\"iso639_2\": \"sqi\",\"name\": \"Albanian\",\"nativeName\": \"Shqip\"}],\"translations\": {\"de\": \"Albanien\",\"es\": \"Albania\",\"fr\": \"Albanie\",\"ja\": \"アルバニア\",\"it\": \"Albania\",\"br\": \"Albânia\",\"pt\": \"Albânia\",\"nl\": \"Albanië\",\"hr\": \"Albanija\",\"fa\": \"آلبانی\"},\"flag\": \"https://restcountries.eu/data/alb.svg\",\"regionalBlocs\": [{\"acronym\": \"CEFTA\",\"name\": \"Central European Free Trade Agreement\",\"otherAcronyms\": [],\"otherNames\": []}],\"cioc\": \"ALB\"}]"
                        )
                    )

            getCountries()
            Assert.assertFalse(ui.getOrAwaitValue().error == null)
        }
    }

    @Test fun successLogin(){
        vm?.apply {

            onUsernameChange("TestUser01")
            onPasswordChange("111111")
            onCountryChange("Philippines")

            every { repo?.loginUser(LoginSignUpRequest("TestUser01","111111","Philippines")) } returns
                    Single.just(UserData(1, "TestUser01", "111111", "Philippines"))

            login()
            Assert.assertTrue(ui.getOrAwaitValue().goToMain)
        }
    }

    @Test fun failLogin(){
        vm?.apply {
            onUsernameChange("TestUser01")
            onPasswordChange("111111")
            onCountryChange("Philippines")

            every { repo?.loginUser(LoginSignUpRequest("TestUser01","111111","Philippines")) } returns
                    Single.error(Exception("Error"))

            login()
            Assert.assertTrue(ui.getOrAwaitValue().error != null)
        }
    }
}