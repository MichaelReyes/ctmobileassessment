package technologies.android.ctmobileassesment.feature.authentication.signup

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.extension.configureInterceptorWithEmpty
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.authentication.LoginSignUpRequest
import technologies.android.ctmobileassesment.core.network.repository.AuthenticationRepository
import javax.inject.Inject

@SuppressLint("CheckResult")
class SignUpViewModel @Inject constructor(
    private val context: Context,
    private val repo: AuthenticationRepository
): ViewModel() {

    private val disposables = CompositeDisposable()

    private var hasInternetConnection = true

    fun setConnectionStream(stream: PublishRelay<Boolean>?){
        stream?.apply {
            this.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    hasInternetConnection = it
                }.addTo(disposables)
        }
    }

    val form = MutableLiveData<SignUpForm>()

    private val autoCompletePublishSubjectUsername = PublishSubject.create<String>()
    private val autoCompletePublishSubjectPassword = PublishSubject.create<String>()
    private val autoCompletePublishSubjectCountry = PublishSubject.create<String>()

    // <editor-fold desc="For field validations">
    init {
        autoCompletePublishSubjectUsername.configureInterceptorWithEmpty(0)
            .subscribe { result -> filteredUsername(result) }
        autoCompletePublishSubjectPassword.configureInterceptorWithEmpty(0)
            .subscribe { result -> filteredPassword(result) }
        autoCompletePublishSubjectCountry.configureInterceptorWithEmpty(0)
            .subscribe { result -> filteredCountry(result) }

        form.value = SignUpForm()
    }

    fun onUsernameChange(text: CharSequence) {
        autoCompletePublishSubjectUsername.onNext(text.toString())
    }

    private fun filteredUsername(result: String) {
        form.value?.apply {
            username = result
            usernameValid = result.isNotEmpty()
            form.value = this
        }
    }

    fun onPasswordChange(text: CharSequence) {
        autoCompletePublishSubjectPassword.onNext(text.toString())
    }

    private fun filteredPassword(result: String) {
        form.value?.apply {
            password = result
            passwordValid = result.isNotEmpty() && result.length > 5
            form.value = this
        }
    }

    fun onCountryChange(text: CharSequence) {
        autoCompletePublishSubjectCountry.onNext(text.toString())
    }

    private fun filteredCountry(result: String) {
        form.value?.apply {
            country = result
            countryValid = result.isNotEmpty()
            form.value = this
        }
    }

    private fun setSigningUp(value: Boolean){
        form.value?.apply {
            signingUp = value
            form.value = this
        }
    }
    // </editor-fold>


    // <editor-fold desc="For UI updates">
    var ui = MutableLiveData(SignUpUi())

    private fun setLoading(value: Boolean){
        ui.value?.apply {
            loading = value
            ui.value = this
        }
    }

    fun setError(value: String?){
        ui.value?.apply {
            error = value
            ui.value = this
        }
    }

    fun setGoToLogin(value: Boolean){
        ui.value?.apply {
            goToLogin = value
            ui.value = this
        }
    }

    private fun setCountries(value: List<Country>?){
        ui.value?.apply {
            countries = value
            ui.value = this
        }
    }

    fun setShowCountriesDialog(value: Boolean){
        ui.value?.apply {
            showCountriesDialog = value
            ui.value = this
        }
    }

    // </editor-fold>

    // <editor-fold desc="For Login">
    fun signUp(){
        form.value?.apply {
            repo.signUp(
                LoginSignUpRequest(username, password, country)
            ).doOnSubscribe {
                setLoading(true)
            }.subscribe({
                setLoading(false)
                setGoToLogin(true)
                setSigningUp(false)
            },{
                setSigningUp(false)
                setError(it.localizedMessage?:context.getString(R.string.error_generic))
                setLoading(false)
            }).addTo(disposables)

            setSigningUp(true)
        }
    }


    // </editor-fold>

    // <editor-fold desc="For Getting countries">

    fun getCountries(){
        if(hasInternetConnection)
            repo.getCountries()
                .doOnSubscribe { setLoading(true) }
                .subscribe({
                    setLoading(false)
                    setCountries(it)
                },{
                    setError(it.localizedMessage?:context.getString(R.string.error_generic))
                    setLoading(false)
                }).addTo(disposables)
        else
            setError(context.getString(R.string.error_no_internet_connection))

    }

    // </editor-fold>



    override fun onCleared() {
        super.onCleared()
    }
}