package technologies.android.ctmobileassesment.feature.authentication.signup

data class SignUpForm(
    var username: String = "",
    var password: String = "",
    var usernameValid: Boolean = false,
    var passwordValid: Boolean = false,
    var country: String = "",
    var countryValid: Boolean = false,
    var signingUp: Boolean = false
)