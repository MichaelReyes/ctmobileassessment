package technologies.android.ctmobileassesment.feature.authentication.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.ItemListener
import com.orhanobut.hawk.Hawk
import technologies.android.ctmobileassesment.App
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseActivity
import technologies.android.ctmobileassesment.core.base.BaseFragment
import technologies.android.ctmobileassesment.core.extension.observe
import technologies.android.ctmobileassesment.core.extension.showListSingleChoiceDialog
import technologies.android.ctmobileassesment.core.extension.viewModel
import technologies.android.ctmobileassesment.core.model.Constants
import technologies.android.ctmobileassesment.core.model.db.data.UserData
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.databinding.FragmentLoginBinding
import technologies.android.ctmobileassesment.feature.dashboard.DashboardActivity
import javax.inject.Inject

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    override val layoutRes = R.layout.fragment_login

    private val countryList = mutableListOf<Country>()

    @Inject
    lateinit var vm:LoginViewModel

    override fun onCreated(savedInstance: Bundle?) {

        (activity as BaseActivity<*>).setToolbar(show = true, showBackButton = false, title = getString(R.string.title_login))

        initObserver()

        Hawk.get<UserData>(Constants.PREF_KEY_USER_DATA)?.run {
            (activity as? BaseActivity<*>)?.apply { goToActivity(this, DashboardActivity::class.java, true)  }
        }
    }

    private fun initObserver() {
        vm = viewModel(viewModelFactory){
            activity?.application?.apply { setConnectionStream((this as App).internetConnectionStream) }
            getCountries()
            observe(ui, ::handleUiUpdates)
        }

        binding.lifecycleOwner = this
        binding.vm = vm
    }

    private fun handleUiUpdates(loginUi: LoginUi?) {
        loginUi?.apply {
            (activity as BaseActivity<*>).showLoading(loading)

            error?.apply {
                showMessage(this, false)
                vm.setError(null)
            }

            countries?.apply {
                countryList.clear()
                countryList.addAll(this)
            }

            if(goToMain){
                showMessage("Login success", true)
                (activity as? BaseActivity<*>)?.apply { goToActivity(this, DashboardActivity::class.java, true)  }
                vm.setGoToMain(false)
            }

            if(goToSignUp){
                findNavController().navigate(R.id.action_goto_signup)
                vm.setGoToSignUp(false)
            }

            if(showCountriesDialog){
                context?.apply {
                    showListSingleChoiceDialog(
                        getString(R.string.label_select_country),
                        getString(R.string.action_cancel),
                        countryList.map { it.name },
                        object: ItemListener{
                            override fun invoke(
                                dialog: MaterialDialog,
                                index: Int,
                                text: CharSequence
                            ) {
                                val country = countryList[index]
                                vm.onCountryChange(country.name)
                            }
                        }
                    ).negativeButton { vm.getCountries() }
                }
                vm.setShowCountriesDialog(false)
            }
        }
    }
}