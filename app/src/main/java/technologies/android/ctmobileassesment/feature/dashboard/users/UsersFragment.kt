package technologies.android.ctmobileassesment.feature.dashboard.users

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.list_users.*
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseActivity
import technologies.android.ctmobileassesment.core.base.BaseFragment
import technologies.android.ctmobileassesment.core.extension.observe
import technologies.android.ctmobileassesment.core.extension.viewModel
import technologies.android.ctmobileassesment.core.model.diffutil.UsersDiffUtilItemCallback
import technologies.android.ctmobileassesment.databinding.FragmentUsersBinding
import technologies.android.ctmobileassesment.feature.dashboard.details.UserDetailsActivity
import technologies.android.ctmobileassesment.feature.dashboard.details.UserDetailsFragment
import javax.inject.Inject

class UsersFragment : BaseFragment<FragmentUsersBinding>() {

    lateinit var adapter: UsersPagedListAdapter

    @Inject
    lateinit var vm: UsersViewModel

    private var twoPane = false

    override val layoutRes = R.layout.fragment_users

    override fun onCreated(savedInstance: Bundle?) {
        twoPane = baseView.findViewById<ViewGroup>(R.id.nav_host_fragment) != null

        initViews()
        initObservers()
    }

    private fun initObservers() {
        vm = viewModel(viewModelFactory){
            getUsers()
            observe(users){ adapter.submitList(it) }
            observe(loading){ it?.apply { (activity as? BaseActivity<*>)?.showLoading(it) } }
        }
    }

    private fun initViews() {
        adapter = UsersPagedListAdapter(UsersDiffUtilItemCallback())
        rvUsers.adapter = adapter

        adapter.clickListener = {
            if(twoPane){
                val navHostFragment =
                    childFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

                navHostFragment.navController.navigate(
                    R.id.userDetailsFragment,
                    bundleOf(Pair(UserDetailsFragment.ARGS_USER, gson.toJson(it)))
                )
            }else{
                (activity as? BaseActivity<*>)?.run {
                    goToActivity(
                        this, UserDetailsActivity::class.java, false,
                        bundleOf(
                            Pair(UserDetailsActivity.EXTRA_USER, gson.toJson(it))
                        )
                    )
                }
            }
        }
    }

}