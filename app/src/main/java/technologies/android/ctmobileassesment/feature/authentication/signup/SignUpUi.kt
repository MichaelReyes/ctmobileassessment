package technologies.android.ctmobileassesment.feature.authentication.signup

import technologies.android.ctmobileassesment.core.model.pojo.Country

data class SignUpUi(
    var loading: Boolean = false,
    var error: String? =  null,
    var goToLogin : Boolean = false,
    var countries: List<Country>? = null,
    var showCountriesDialog: Boolean = false
)
