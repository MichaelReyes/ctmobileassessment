package technologies.android.ctmobileassesment.feature.dashboard.users

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.databinding.ItemUserBinding

class UsersPagedListAdapter constructor(
    @NonNull val diffCallback: DiffUtil.ItemCallback<User>
): PagedListAdapter<User, RecyclerView.ViewHolder>(diffCallback)  {

    internal var clickListener: (User) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemUserBinding>(LayoutInflater.from(parent.context),
            R.layout.item_user, parent, false)
        return Holder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as Holder
        viewHolder.bind(getItem(position)!!)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding: ItemUserBinding? = DataBindingUtil.bind(itemView)

        internal fun bind(user: User) {
            binding?.apply {
                this.user = user
                itemView.setOnClickListener { clickListener(user) }
                executePendingBindings()
                ContextCompat.getDrawable(itemView.context, R.drawable.ic_user)?.run {
                    ivImage.setImageFromDrawable(this)
                }
            }
        }
    }
}

