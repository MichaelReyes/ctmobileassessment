package technologies.android.ctmobileassesment.feature.dashboard.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import technologies.android.ctmobileassesment.core.model.datasource.UsersDataSourceFactory
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.core.network.repository.UsersRepository
import javax.inject.Inject

class UsersViewModel @Inject constructor(
    private val repo: UsersRepository
): ViewModel() {

    lateinit var sourceFactory: UsersDataSourceFactory

    var empty = MutableLiveData(true)
    var users: LiveData<PagedList<User>> = MutableLiveData()
    var loading = MutableLiveData(false)

    fun getUsers(){
        val config = PagedList.Config.Builder()
            .setPageSize(5)
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(5)
            .build()

        sourceFactory = UsersDataSourceFactory(repo, loading, empty)

        users = LivePagedListBuilder(sourceFactory, config).build()
    }

    override fun onCleared() {
        super.onCleared()
    }
}