package technologies.android.ctmobileassesment.feature.dashboard.details

import android.os.Bundle
import android.view.ViewGroup
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseMapFragment
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.databinding.FragmentUserDetailsBinding

class UserDetailsFragment : BaseMapFragment<FragmentUserDetailsBinding>() {

    override val layoutRes = R.layout.fragment_user_details

    override fun onCreated(savedInstance: Bundle?) {

        binding.lifecycleOwner = this
        checkArgs()
        onMapParentFragmentCreated()
    }

    private fun checkArgs() {
        arguments?.apply {
            if(containsKey(ARGS_USER))
                binding.user = gson.fromJson(getString(ARGS_USER), User::class.java)
            else
                binding.user = null
        }
    }

    override fun getMapLayout(): ViewGroup = baseView.findViewById(R.id.layoutMap)

    override fun getMapFragment(): SupportMapFragment? {
        return childFragmentManager.findFragmentById(R.id.fragmentMap) as SupportMapFragment?
    }

    override fun onMapReady() {
        binding.user?.apply {
            showMarker(LatLng(this.address.geo.lat.toDouble(), this.address.geo.lng.toDouble()), this.name, this.address.suite)
        }
    }

    companion object{
        const val ARGS_USER = "_args_user"
    }

}