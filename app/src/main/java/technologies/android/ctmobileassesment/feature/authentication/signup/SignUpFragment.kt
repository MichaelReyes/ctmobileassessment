package technologies.android.ctmobileassesment.feature.authentication.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.ItemListener
import technologies.android.ctmobileassesment.App
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseActivity
import technologies.android.ctmobileassesment.core.base.BaseFragment
import technologies.android.ctmobileassesment.core.extension.observe
import technologies.android.ctmobileassesment.core.extension.showListSingleChoiceDialog
import technologies.android.ctmobileassesment.core.extension.viewModel
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.databinding.FragmentSignUpBinding
import technologies.android.ctmobileassesment.feature.authentication.login.LoginUi
import javax.inject.Inject

class
SignUpFragment : BaseFragment<FragmentSignUpBinding>() {

    @Inject
    lateinit var vm: SignUpViewModel

    private val countryList = mutableListOf<Country>()

    override val layoutRes = R.layout.fragment_sign_up

    override fun onCreated(savedInstance: Bundle?) {
        (activity as BaseActivity<*>).setToolbar(show = true, showBackButton = true, title = getString(R.string.title_sign_up))

        initObserver()
    }

    private fun initObserver(){
        vm = viewModel(viewModelFactory){
            activity?.application?.apply { setConnectionStream((this as App).internetConnectionStream) }
            getCountries()
            observe(ui, ::handleUiUpdates)
        }


        binding.lifecycleOwner = this
        binding.vm = vm
    }

    private fun handleUiUpdates(loginUi: SignUpUi?) {
        loginUi?.apply {
            (activity as BaseActivity<*>).showLoading(loading)

            error?.apply {
                showMessage(this, false)
                vm.setError(null)
            }

            countries?.apply {
                countryList.clear()
                countryList.addAll(this)
            }

            if(goToLogin){
                showMessage(getString(R.string.message_sign_up_successful), true)
                findNavController().navigateUp()
            }

            if(showCountriesDialog){
                context?.apply {
                    showListSingleChoiceDialog(
                        getString(R.string.label_select_country),
                        getString(R.string.action_cancel),
                        countryList.map { it.name },
                        object: ItemListener{
                            override fun invoke(
                                dialog: MaterialDialog,
                                index: Int,
                                text: CharSequence
                            ) {
                                val country = countryList[index]
                                vm.onCountryChange(country.name)
                            }
                        }
                    ).negativeButton { vm.getCountries() }
                }
                vm.setShowCountriesDialog(false)
            }
        }
    }
}