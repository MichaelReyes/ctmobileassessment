package technologies.android.ctmobileassesment.feature.dashboard.details

import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_user_details.*
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseMapActivity
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.databinding.ActivityUserDetailsBinding

class UserDetailsActivity : BaseMapActivity<ActivityUserDetailsBinding>() {

    override val layoutRes: Int
        get() = R.layout.activity_user_details

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
        binding.lifecycleOwner = this
        checkExtras()
        setToolbar(show = true, showBackButton = true)
        initViews()
        onMapParentActivityCreated()
    }

    private fun initViews(){
        binding.user?.apply {
            setToolbar(show = true, showBackButton = true, title = this.name)
        }
    }

    private fun checkExtras() {
        intent.extras?.apply {
            if(containsKey(EXTRA_USER)){
                binding.user = gson.fromJson(this.getString(EXTRA_USER), User::class.java)
            }else
                finish()
        }?: finish()
    }

    override fun getMapLayout(): ViewGroup = findViewById(R.id.layoutMap)

    override fun getMapFragment(): SupportMapFragment? {
        return supportFragmentManager.findFragmentById(R.id.fragmentMap) as SupportMapFragment?
    }

    override fun getCurrentActivity(): FragmentActivity = this@UserDetailsActivity

    override fun onMapReady() {
        binding.user?.apply {
            showMarker(LatLng(this.address.geo.lat.toDouble(), this.address.geo.lng.toDouble()), this.name, this.address.suite)
        }
    }

    companion object{
        const val EXTRA_USER = "_extra_user"
    }
}