package technologies.android.ctmobileassesment.feature.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_authentication.*
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.base.BaseActivity
import technologies.android.ctmobileassesment.databinding.ActivityAuthenticationBinding

class AuthenticationActivity : BaseActivity<ActivityAuthenticationBinding>() {

    override val layoutRes = R.layout.activity_authentication

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
    }

}