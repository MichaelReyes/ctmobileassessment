package technologies.android.ctmobileassesment.feature.authentication.login

import technologies.android.ctmobileassesment.core.model.pojo.Country

data class LoginUi(
    var loading: Boolean = false,
    var error: String? =  null,
    var goToMain : Boolean = false,
    var goToSignUp : Boolean = false,
    var countries: List<Country>? = null,
    var showCountriesDialog: Boolean = false
)
