package technologies.android.ctmobileassesment.feature.authentication.login

data class LoginForm(
    var username: String = "",
    var password: String = "",
    var usernameValid: Boolean = false,
    var passwordValid: Boolean = false,
    var country: String = "",
    var countryValid: Boolean = false,
    var signingIn: Boolean = false
)