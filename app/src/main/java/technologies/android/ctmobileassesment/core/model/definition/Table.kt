package technologies.android.ctmobileassesment.core.model.definition

@Retention(AnnotationRetention.RUNTIME)
annotation class Table {
    companion object {
        const val USERS = "TBL_USERS"
    }
}