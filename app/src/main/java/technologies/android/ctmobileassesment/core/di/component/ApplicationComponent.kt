package technologies.android.ctmobileassesment.core.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import technologies.android.ctmobileassesment.App
import technologies.android.ctmobileassesment.core.di.module.*
import technologies.android.ctmobileassesment.core.di.module.vm.ViewModelModule
import technologies.android.ctmobileassesment.core.di.scope.AppScope


@AppScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        NetworkModule::class,
        DatabaseModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory: AndroidInjector.Factory<App>
}