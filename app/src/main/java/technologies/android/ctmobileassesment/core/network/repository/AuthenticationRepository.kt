package technologies.android.ctmobileassesment.core.network.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import technologies.android.ctmobileassesment.core.model.db.dao.UserDataDao
import technologies.android.ctmobileassesment.core.model.db.data.UserData
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.authentication.LoginSignUpRequest
import technologies.android.ctmobileassesment.core.network.AppApi
import javax.inject.Inject
import javax.inject.Named

class AuthenticationRepository @Inject constructor(
    @Named("Countries") private val service: AppApi,
    private val userDao: UserDataDao
) {

    fun loginUser(request: LoginSignUpRequest): Single<UserData>{
        return userDao.getUserByLoginCredentials(
            request.username, request.password, request.country
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun signUp(request: LoginSignUpRequest): Single<UserData>{
        return userDao.insert(UserData(username = request.username, password = request.password, country = request.country))
            .andThen( userDao.getUserByUsername(request.username) )
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getCountries(): Single<List<Country>>{
        return service.getCountries().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    }


}