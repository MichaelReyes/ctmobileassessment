package technologies.android.ctmobileassesment.core.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import technologies.android.ctmobileassesment.core.di.scope.AppScope
import technologies.android.ctmobileassesment.core.model.db.AppDatabase
import technologies.android.ctmobileassesment.core.model.db.dao.UserDataDao

@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideUserDataDao(appDataBase: AppDatabase): UserDataDao {
        return appDataBase.userDataDao()
    }

}