package technologies.android.ctmobileassesment.core.model.db.dao

import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Maybe
import io.reactivex.Single
import technologies.android.ctmobileassesment.core.model.db.data.UserData
import technologies.android.ctmobileassesment.core.model.definition.Table
import technologies.android.ctmobileassesment.core.model.pojo.Country

@Dao
interface UserDataDao: BaseDao<UserData> {

    @Query("SELECT * from ${Table.USERS} WHERE username = :username")
    fun getUserByUsername(username: String): Single<UserData>

    @Query("SELECT * from ${Table.USERS} WHERE username = :username AND password = :password AND country = :country")
    fun getUserByLoginCredentials(username: String, password: String, country: String): Single<UserData>
}