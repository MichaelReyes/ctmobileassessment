package technologies.android.ctmobileassesment.core.network.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import technologies.android.ctmobileassesment.core.model.db.dao.UserDataDao
import technologies.android.ctmobileassesment.core.model.db.data.UserData
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.authentication.LoginSignUpRequest
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.core.network.AppApi
import javax.inject.Inject
import javax.inject.Named

class UsersRepository @Inject constructor(
    @Named("Backend") private val service: AppApi
) {

    fun getUsers(start: Int, end: Int): Maybe<List<User>> {
        return service.getUsers(start, end).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}