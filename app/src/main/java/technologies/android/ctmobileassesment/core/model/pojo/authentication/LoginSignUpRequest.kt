package technologies.android.ctmobileassesment.core.model.pojo.authentication

data class LoginSignUpRequest(
    val username: String,
    val password: String,
    val country: String
)