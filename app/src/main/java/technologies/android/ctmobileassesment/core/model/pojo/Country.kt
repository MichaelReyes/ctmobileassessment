package technologies.android.ctmobileassesment.core.model.pojo

data class Country(
    val capital: String,
    val name: String,
    val region: String,
    val subregion: String
)