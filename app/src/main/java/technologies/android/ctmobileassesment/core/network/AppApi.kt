package technologies.android.ctmobileassesment.core.network

import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import technologies.android.ctmobileassesment.core.model.pojo.Country
import technologies.android.ctmobileassesment.core.model.pojo.users.User

interface AppApi {

    @GET("all")
    fun getCountries(): Single<List<Country>>

    @GET("users")
    fun getUsers(@Query("_start") start: Int, @Query("_end") end: Int): Maybe<List<User>>

}