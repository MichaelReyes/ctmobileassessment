package technologies.android.ctmobileassesment.core.utility;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public  class TouchableWrapper extends FrameLayout {

	private long lastTouched = 0;
	private static final long SCROLL_TIME = 200L;
	private UpdateMapAfterUserInteraction updateMapAfterUserInteraction;
	private Handler mHandler;


	public TouchableWrapper(Context context, UpdateMapAfterUserInteraction updateMapAfterUserInteraction) {
		super(context);
		this.updateMapAfterUserInteraction = updateMapAfterUserInteraction;
		mHandler = new Handler();
	}

	public boolean isTouched = false;
	private Runnable mRunnable = () -> isTouched = false;

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			lastTouched = SystemClock.uptimeMillis();
			isTouched = true;
			mHandler.removeCallbacks(mRunnable);
			break;
		case MotionEvent.ACTION_UP:
			final long now = SystemClock.uptimeMillis();
			if (now - lastTouched > SCROLL_TIME) {
				// Update the map
				updateMapAfterUserInteraction.onUpdateMapAfterUserInteraction();
			}
			mHandler.postDelayed(mRunnable, 5000);
			break;
		}
		return super.dispatchTouchEvent(ev);
	}

	// Map Activity must implement this interface
    public interface UpdateMapAfterUserInteraction {
        public void onUpdateMapAfterUserInteraction();
    }
}