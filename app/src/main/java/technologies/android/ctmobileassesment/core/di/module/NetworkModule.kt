package technologies.android.ctmobileassesment.core.di.module

import androidx.annotation.NonNull
import com.orhanobut.hawk.Hawk

import java.util.concurrent.TimeUnit

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import technologies.android.ctmobileassesment.BuildConfig
import technologies.android.ctmobileassesment.core.di.scope.AppScope
import technologies.android.ctmobileassesment.core.model.Constants
import technologies.android.ctmobileassesment.core.network.AppApi
import javax.inject.Named

const val BACKEND_BASE_URL = "https://jsonplaceholder.typicode.com/"
//Getting country list from another API source
const val COUNTRIES_BASE_URL = "https://restcountries.eu/rest/v2/"

@Module
class NetworkModule {

    internal val loggingInterceptor: HttpLoggingInterceptor
        @Provides
        get() = HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        )

    @Provides
    @Named("Backend")
    fun getBackendApiEndpoint(): String {
        return BACKEND_BASE_URL
    }

    @Provides
    @Named("Countries")
    fun getCountriesApiEndpoint(): String {
        return COUNTRIES_BASE_URL
    }

    @Provides
    @Named("Backend")
    @AppScope
    internal fun provideApiRetrofit(
        @Named("Backend") @NonNull baseUrl: String, client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }


    @Provides
    @Named("Countries")
    @AppScope
    internal fun provideCountriesApiRetrofit(
        @Named("Countries") @NonNull baseUrl: String, client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @AppScope
    internal fun getHttpClient(
        interceptor: HttpLoggingInterceptor
        /*@Named("Backend") authenticator: Authenticator*/
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder
            /*.authenticator(authenticator)*/
            .addInterceptor(httpApiInterceptor())
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)

            .retryOnConnectionFailure(true)

        builder.addInterceptor(interceptor)

        return builder.build()
    }

    @Provides
    @Named("Backend")
    @AppScope
    fun getAuthenticator(): Authenticator {
        return object : Authenticator {
            override fun authenticate(route: Route?, response: Response): Request? {
                return when {
                    response.request.header("Authorization") != null -> null
                    else -> response.request.newBuilder().header(
                        "Authorization",
                        "Bearer " + Hawk.get(Constants.PREF_KEY_ACCESS_TOKEN, "")
                    ).build()

                }
            }
        }
    }

    private fun httpApiInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .header("Content-Type", "application/json")
                    /* Will comment this out since not needed. But this is how I handle access token for API calls
                .header(
                    "Authorization",
                    when {
                        Hawk.get(Constants.PREF_KEY_ACCESS_TOKEN, "").isEmpty() -> "Authorization:"
                        else -> "Bearer " + Hawk.get<String>(Constants.PREF_KEY_ACCESS_TOKEN)
                    }
                )
                    */
                .build()
            chain.proceed(request)
        }
    }

    @Provides
    @Named("Backend")
    @AppScope
    fun getBackendApiService(@Named("Backend") retrofit: Retrofit): AppApi {
        return retrofit.create(AppApi::class.java)
    }

    @Provides
    @Named("Countries")
    @AppScope
    fun getCountriesApiService(@Named("Countries") retrofit: Retrofit): AppApi {
        return retrofit.create(AppApi::class.java)
    }

}

