package technologies.android.ctmobileassesment.core.base

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener.Builder.withContext
import technologies.android.ctmobileassesment.R
import technologies.android.ctmobileassesment.core.utility.TouchableWrapper


abstract class BaseMapFragment<V: ViewDataBinding> : BaseFragment<V>(), OnMapReadyCallback,
    TouchableWrapper.UpdateMapAfterUserInteraction  {

    protected var map: GoogleMap? = null
    protected var mpFragment: SupportMapFragment? = null
    private var touchView: TouchableWrapper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun onMapParentFragmentCreated() {
        checkLocationPermissions()
    }

    private fun checkLocationPermissions() {

        val permissions = mutableListOf<String>()
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)

        activity?.apply {
            Dexter.withActivity(this)
                .withPermissions(
                    permissions
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            initMapAndLocation()
                        } else {
                            checkLocationPermissions()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                    }
                }).check()
        }
    }

    abstract fun getMapLayout(): ViewGroup

    abstract fun getMapFragment(): SupportMapFragment?

    private fun initMapAndLocation() {
        initMap()
    }

    private fun initMap() {
        context?.let { c ->
            touchView = TouchableWrapper(c, this)
            mpFragment = getMapFragment()
            mpFragment?.apply {
                getMapAsync(this@BaseMapFragment)
                val mapView = view
                if (mapView != null && mapView.parent != null) (mapView.parent as ViewGroup).removeView(mapView)
                touchView?.addView(mapView)
            }

            val layoutMap = getMapLayout()
            layoutMap.addView(touchView, 0)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map?.mapType = GoogleMap.MAP_TYPE_NORMAL
        moveMapCameraToLocation(LatLng(38.907292, -77.036892))
        onMapReady()
    }

    private fun moveMapCameraToLocation(latLng: LatLng) {
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16.0f)
        map!!.animateCamera(cameraUpdate)
    }

    protected fun showMarker(location: LatLng, title: String, address: String){
        map?.apply {

            val bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.ic_location)

            val marker = addMarker(MarkerOptions().position(location).title(title).snippet(address).icon(bitmapDescriptor))

            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, 10f)
            animateCamera(cameraUpdate)
        }
    }

    override fun onUpdateMapAfterUserInteraction() {}

    abstract fun onMapReady()
}