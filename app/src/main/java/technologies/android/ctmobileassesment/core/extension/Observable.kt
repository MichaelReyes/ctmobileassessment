package technologies.android.ctmobileassesment.core.extension

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

fun <T> PublishSubject<T>.configureInterceptorWithEmpty(timeout: Long): Observable<T>
        = this.debounce(timeout, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(
    AndroidSchedulers.mainThread())