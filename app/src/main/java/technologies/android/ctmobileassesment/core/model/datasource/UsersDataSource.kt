package technologies.android.ctmobileassesment.core.model.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.rxkotlin.addTo
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.core.network.repository.UsersRepository

class UsersDataSource(private val repository: UsersRepository,
                      private val loading: MutableLiveData<Boolean>,
                      private val empty: MutableLiveData<Boolean>) : ItemKeyedDataSource<Int, User>() {

    private var disposables = CompositeDisposable()
    private var pageNumber = 0
    private var size = 5

    var params: LoadParams<Int>? = null
    var callback: LoadCallback<User>? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<User>
    ) {
        repository.getUsers(pageNumber, size)
            .doOnSubscribe { loading.postValue(true) }
            .subscribeWith(object : DisposableMaybeObserver<List<User>>() {

                override fun onComplete() {
                    loading.postValue(false)
                }

                override fun onSuccess(users: List<User>) {
                    onUsersFetched(users, callback )
                    loading.postValue(false)
                }

                override fun onError(e: Throwable) {
                    onCallError(e)
                    loading.postValue(false)
                }


        }).addTo(disposables)
    }

    private fun onUsersFetched(users: List<User>, callback: LoadInitialCallback<User>) {
        pageNumber+=size
        empty.postValue(users.isEmpty())
        callback.onResult(users)
    }

    private fun onCallError(throwable: Throwable) {
        throwable.printStackTrace()
        empty.postValue(true)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<User>) {
        this.params = params
        this.callback = callback

        repository.getUsers(params.key, params.key + size)
            .doOnSubscribe { loading.postValue(true) }
            .subscribeWith(object : DisposableMaybeObserver<List<User>>() {

                override fun onComplete() {
                    loading.postValue(false)
                }

                override fun onSuccess(users: List<User>) {
                    onMoreNewsFetched(users, callback )
                    loading.postValue(false)
                }

                override fun onError(e: Throwable) {
                    onPaginationError(e)
                    loading.postValue(false)
                }


            }).addTo(disposables)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<User>) {}

    override fun getKey(item: User): Int = pageNumber

    private fun onMoreNewsFetched(users: List<User>, callback: LoadCallback<User>) {
        pageNumber+=size
        callback.onResult(users)
    }

    private fun onPaginationError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    fun clear() {
        pageNumber = 0
        disposables.clear()
    }
}
