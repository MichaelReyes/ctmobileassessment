package technologies.android.ctmobileassesment.core.model.db.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import technologies.android.ctmobileassesment.core.model.definition.Table

@Entity(tableName = Table.USERS)
class UserData(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0,

    @ColumnInfo(name = "username")
    var username: String? = "",

    @ColumnInfo(name = "password")
    var password: String? = "",

    @ColumnInfo(name = "country")
    var country: String? = ""
)