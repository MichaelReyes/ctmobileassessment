package technologies.android.ctmobileassesment.core.model.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import technologies.android.ctmobileassesment.core.model.pojo.users.User
import technologies.android.ctmobileassesment.core.network.repository.UsersRepository
import javax.inject.Inject


class UsersDataSourceFactory  @Inject constructor(
    private val repo: UsersRepository,
    private val loading: MutableLiveData<Boolean>,
    private val empty: MutableLiveData<Boolean>
) : DataSource.Factory<Int, User>() {

    val source = MutableLiveData<UsersDataSource>()

    override fun create(): DataSource<Int, User> {
        val source = UsersDataSource(repo, loading, empty)
        this.source.postValue(source)
        return source
    }
}
