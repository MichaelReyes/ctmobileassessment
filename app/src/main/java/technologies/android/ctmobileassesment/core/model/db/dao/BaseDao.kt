package technologies.android.ctmobileassesment.core.model.db.dao

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg t: T): Completable

    @Update
    fun update(vararg t: T): Completable

    @Delete
    fun delete(vararg t: T): Completable
}