package technologies.android.ctmobileassesment.core.base

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import technologies.android.ctmobileassesment.R
import java.io.File

class GlideImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
    AppCompatImageView(context, attrs, defStyle) {

    fun setImageUrl(imageUrl: String?) {
        if (imageUrl == null)
            return
        Glide.with(context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .into(this)
    }

    fun setImageUri(imageUri: Uri?) {
        if (imageUri == null)
            return
        Glide.with(context)
            .load(imageUri)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .into(this)
    }

    fun setImageFile(file: File?) {
        if (file == null)
            return
        Glide.with(context)
            .load(file)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder)))
            .into(this)
    }

    fun setImageFromDrawable(drawable: Drawable) {
        Glide.with(context)
            .load(drawable)
            .apply(
                RequestOptions()
                    .transform(CenterCrop(), RoundedCorners(64))
                    .placeholder(ContextCompat.getDrawable(context, R.drawable.img_placeholder))
            )
            .into(this)
    }

    fun setImageBitmap(bitmap: Bitmap, placeholder: Drawable) {
        Glide.with(context)
            .load(bitmap)
            .into(this)
    }

}
