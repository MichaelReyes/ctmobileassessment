package technologies.android.ctmobileassesment.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import technologies.android.ctmobileassesment.core.di.scope.FragmentScope
import technologies.android.ctmobileassesment.feature.authentication.login.LoginFragment
import technologies.android.ctmobileassesment.feature.authentication.signup.SignUpFragment
import technologies.android.ctmobileassesment.feature.dashboard.details.UserDetailsFragment
import technologies.android.ctmobileassesment.feature.dashboard.users.UsersFragment

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindLoginFragment(): LoginFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindSignUpFragment(): SignUpFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUsersFragment(): UsersFragment


    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsFragment(): UserDetailsFragment

}