package technologies.android.ctmobileassesment.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import technologies.android.ctmobileassesment.core.di.scope.ActivityScope
import technologies.android.ctmobileassesment.feature.authentication.AuthenticationActivity
import technologies.android.ctmobileassesment.feature.dashboard.DashboardActivity
import technologies.android.ctmobileassesment.feature.dashboard.details.UserDetailsActivity

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindAuthenticationActivity(): AuthenticationActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindUserDetailsActivity(): UserDetailsActivity

}