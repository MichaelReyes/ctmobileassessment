package technologies.android.ctmobileassesment.core.model.diffutil

import androidx.recyclerview.widget.DiffUtil
import technologies.android.ctmobileassesment.core.model.pojo.users.User

class UsersDiffUtilItemCallback : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.email == newItem.email
    }
}
