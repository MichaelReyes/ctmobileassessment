package technologies.android.ctmobileassesment.core.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import technologies.android.ctmobileassesment.App
import technologies.android.ctmobileassesment.core.di.scope.AppScope

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(app: App): Context {
        return app
    }

}