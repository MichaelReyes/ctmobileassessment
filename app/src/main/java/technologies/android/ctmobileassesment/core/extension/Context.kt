package technologies.android.ctmobileassesment.core.extension

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.ItemListener
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import technologies.android.ctmobileassesment.R

fun Context.showListSingleChoiceDialog(title: String, negativeLabel: String, items: List<String>, listener: ItemListener, initialSelection: Int = -1): MaterialDialog {
    return MaterialDialog(this).show {
        title(text = title)
        positiveButton(R.string.action_select)
        negativeButton(text = negativeLabel)
        listItemsSingleChoice(items = items, selection = listener, initialSelection = initialSelection)
    }
}