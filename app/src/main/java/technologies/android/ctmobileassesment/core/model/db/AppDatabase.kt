package technologies.android.ctmobileassesment.core.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import technologies.android.ctmobileassesment.core.model.db.dao.UserDataDao
import technologies.android.ctmobileassesment.core.model.db.data.UserData

@Database(entities = [ UserData::class ],
        version = AppDatabase.VERSION)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "ctassessmentapp.db"
        const val VERSION = 1
    }

    abstract fun userDataDao(): UserDataDao
}